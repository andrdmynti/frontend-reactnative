import React from 'react';
import { StyleSheet, View, Image, Text, KeyboardAvoidingView } from 'react-native';
import LoginForm from './LoginForm';


export default class Login extends React.Component {
  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <View style={styles.logoContainer}>
          <Image 
            style={styles.logo}
            source={require('../../images/pregnant.png')}
          />
          <Text style={styles.title}>Selamat Datang!</Text>
          <Text style={styles.title}>Silahkan Login!</Text>
        </View>
        <View style={styles.formContainer}>
          <LoginForm />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3498db'
  },
  logo: {
    width: 100,
    height: 100
  },
  
  logoContainer: {
    flexGrow: 1,
    backgroundColor: '#3498db',
    alignItems: 'center',
    justifyContent: 'center'
  },
  
  title:{
    color: '#FFF',
    marginTop: 1,
    width: 168, 
    textAlign: 'center',
    opacity: 0.9
  }
});
