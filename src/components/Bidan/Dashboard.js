import React from 'react';
import {
	StyleSheet,
	Text,
	View
} from 'react-native';


export default class Dashboard extends React.Component {
	static navigationOptions = {
		drawerLabel: 'Beranda',
	}
	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.welcome}>
					ini isi dari dashboard
				</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignItems: 'center',
		backgroundColor: '#3498db',
		flex: 1,
		justifyContent: 'center'
	},
});