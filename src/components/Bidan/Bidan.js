import React from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	TextInput,
	Button
} from 'react-native';
import { createDrawerNavigator } from 'react-navigation';
import Dashboard from './Dashboard';
import Redis from './Redis';

const navigation = createDrawerNavigator ({
	Dashboard: {
		screen: Dashboard
	},
	Redis: {
		screen: Redis
	}	
})


export default navigation