import React from 'react';
import {
	StyleSheet,
	Text,
	View
} from 'react-native';


export default class Artikel extends React.Component {
	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.welcome}>
					ini isi dari Artikel untuk Pasien
				</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		flex: 1,
		justifyContent: 'center'
	},
});