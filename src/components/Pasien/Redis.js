import React from 'react';
import {
	StyleSheet,
	Text,
	View
} from 'react-native';


export default class Redis extends React.Component {
	static navigationOptions = {
		drawerLabel: 'History Checkup',
	}
	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.welcome}>
					ini isi dari history checkup Pasien
				</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		flex: 1,
		justifyContent: 'center'
	},
});