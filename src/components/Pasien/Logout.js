import React from 'react';
import {
	StyleSheet,
	Text,
	View
} from 'react-native';


export default class Logout extends React.Component {
	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.welcome}>
					Anda berhasil Logout
				</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		alignItems: 'center',
		backgroundColor: '#FFFFFF',
		flex: 1,
		justifyContent: 'center'
	},
});