import React from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	TextInput,
	Button
} from 'react-native';
import { createDrawerNavigator } from 'react-navigation';
import Dashboard from './Dashboard';
import Redis from './Redis';
import Artikel from './Artikel';
import Logout from './Logout';

const navigation = createDrawerNavigator ({
	Dashboard: {
		screen: Dashboard
	},
	Redis: {
		screen: Redis
	},
	Artikel: {
		screen: Artikel
	}
	,
	Logout: {
		screen: Logout
	}	
})


export default navigation