import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class Splash extends Component {
	render() {
		return (
			<View style={styles.wrapper}>
				<Text style={styles.title}>Hello Erma!</Text>
				<Text style={styles.subtitle}>Semangat ya tjoy! U can!</Text>
				<Text style={styles.subtitle}>Sabar, bentar lagi libur kok.</Text>
				<Text style={styles.subtitle}>Bisa nonton drakor, dracin, mecin apa lah itu sesuka u abis ini</Text>
				<Text style={styles.subtitle}>ehehehehe</Text>
				<Text style={styles.subtitle}>Btw ini adalah romantis dengan modal yang antimainstream.</Text>
				<Text style={styles.subtitle}>Alig.</Text>
				<Text></Text>
				<Text></Text>
				<Text></Text>
				<Text></Text>
				<Text></Text>
				<Text></Text>
				<Text>Created by @andrdmynti</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create ({
	wrapper: { 
		backgroundColor: '#4286f4', 
		flex: 1, 
		justifyContent: 'center',
		alignItems: 'center'
	},
	title: { 
		color: 'white',
		fontSize: 20,
		fontWeight: 'bold'
	},
	subtitle: { 
		color: 'white',
		fontSize: 15,
		alignItems: 'center'
	}
});